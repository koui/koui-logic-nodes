# Koui Logic Nodes Pack (work in progress)

![Screenshot](.gitlab/screenshot.png)

## Installation

- Install [Koui](https://gitlab.com/koui/Koui)
- Open the folder of your Armory project (where the `.blend` file is at)
- Create a `Libraries` subdirectory if it does not exist yet
- Open a command line in that folder
- Clone this repository using `git clone https://gitlab.com/koui/koui-logic-nodes.git`
  Your project directory should now look like this:

  ```
  Project Directory/
  │
  ├── Libraries/
  │    └── koui-logic-nodes/
  │
  ├── Subprojects/
  │    └── Koui/
  │
  └── project.blend
  ```
- Restart Blender, Armory will automatically register the nodes from this package.
