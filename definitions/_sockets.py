import bpy
from bpy.props import EnumProperty

from arm.logicnode import arm_sockets

ELEMENT_SHAPE = "SQUARE"
ENUM_SHAPE = "DIAMOND"


class ArmKouiSocketElement(arm_sockets.ArmCustomSocket):
    """Represents an element of Koui.Element"""
    bl_idname = "ArmKouiSocketElement"
    bl_label = "Element Socket"
    arm_socket_type = 'NONE'

    def __init__(self):
        super()
        self.display_shape = ELEMENT_SHAPE

    def draw(self, context, layout, node, text):
        layout.label(text=self.name)

    def draw_color(self, context, node):
        return 0.78, 0.1, 0.92, 1


class ArmKouiSocketRadioGroup(arm_sockets.ArmCustomSocket):
    """Represents a radio group (koui.utils.RadioGroup)"""
    bl_idname = "ArmKouiSocketRadioGroup"
    bl_label = "Radio Group Socket"
    arm_socket_type = 'NONE'

    def __init__(self):
        super()
        self.display_shape = ELEMENT_SHAPE

    def draw(self, context, layout, node, text):
        layout.label(text=self.name)

    def draw_color(self, context, node):
        return 0.16, 0.64, 0.83, 1


class ArmKouiSocketNumberType(arm_sockets.ArmCustomSocket):
    """Represents a number type for koui.elements.NumberInput"""
    bl_idname = "ArmKouiSocketNumberType"
    bl_label = "Number Type Socket"
    arm_socket_type = 'STRING'

    number_types: EnumProperty(
        items=[
            ("TypeFloat", "Float", "Represents a signed floating point number"),
            ("TypeUnsignedFloat", "Unsigned Float", "Represents an unsigned floating point number"),
            ("TypeInt", "Int", "Represents a signed integer number"),
            ("TypeUnsignedInt", "Unsigned Int", "Represents an unsigned integer number")
        ],
        name="Number Type",
        description="The type of number this NumberInput accepts",
        default="TypeInt"
    )

    def __init__(self):
        super()
        self.display_shape = ENUM_SHAPE

    def get_default_value(self):
        return self.number_types

    def draw(self, context, layout, node, text):
        draw_socket_layout(self, "number_types", layout)

    def draw_color(self, context, node):
        return 0.92, 0.82, 0.37, 1


class ArmKouiSocketAnchor(arm_sockets.ArmCustomSocket):
    """Represents a layout anchor."""
    bl_idname = "ArmKouiSocketAnchor"
    bl_label = "Anchor Socket"
    arm_socket_type = 'STRING'

    anchor: EnumProperty(
        items=[
            ("TopLeft", "Top Left", "TopLeft"),
            ("TopCenter", "Top Center", "TopCenter"),
            ("TopRight", "Top Right", "TopRight"),
            ("MiddleLeft", "Middle Left", "MiddleLeft"),
            ("MiddleCenter", "Middle Center", "MiddleCenter"),
            ("MiddleRight", "Middle Right", "MiddleRight"),
            ("BottomLeft", "Bottom Left", "BottomLeft"),
            ("BottomCenter", "Bottom Center", "BottomCenter"),
            ("BottomRight", "Bottom Right", "BottomRight")
        ],
        name="Anchor",
        description="The layout anchor for this element",
        default="TopLeft"
    )

    def __init__(self):
        super()
        self.display_shape = ENUM_SHAPE

    def get_default_value(self):
        return self.anchor

    def draw(self, context, layout, node, text):
        draw_socket_layout(self, "anchor", layout)

    def draw_color(self, context, node):
        return 0.9, 0.55, 0.29, 1


class ArmKouiSocketMouseButton(arm_sockets.ArmCustomSocket):
    """Represents a mouse button."""
    bl_idname = "ArmKouiSocketMouseButton"
    bl_label = "Mouse Button Socket"
    arm_socket_type = 'INT'

    button: EnumProperty(
        items=[
            ("Left", "Left", "Left mouse button"),
            ("Middle", "Middle", "Middle mouse button"),
            ("Right", "Right", "Right mouse button")
        ],
        name="Mouse Button",
        description="A mouse button",
        default="Left"
    )

    def __init__(self):
        super()
        self.display_shape = ENUM_SHAPE

    def get_default_value(self):
        if self.button == "Left":
            return 0
        if self.button == "Right":
            return 1
        # Middle
        return 2

    def draw(self, context, layout, node, text):
        draw_socket_layout(self, "button", layout)

    def draw_color(self, context, node):
        return 0.18, 0.49, 0.79, 1


class ArmKouiSocketKeyCode(arm_sockets.ArmCustomSocket):
    """Represents a kha.input.KeyCode."""
    bl_idname = "ArmKouiSocketKeyCode"
    bl_label = "Key Code Socket"
    arm_socket_type = 'INT'

    # Identifiers are strings because of Blender, later converted to int
    # KeyCodes can be found here: http://api.kha.tech/kha/input/KeyCode.html
    keycode: EnumProperty(items=[
        ("65", "a", "a"),
        ("66", "b", "b"),
        ("67", "c", "c"),
        ("68", "d", "d"),
        ("69", "e", "e"),
        ("70", "f", "f"),
        ("71", "g", "g"),
        ("72", "h", "h"),
        ("73", "i", "i"),
        ("74", "j", "j"),
        ("75", "k", "k"),
        ("76", "l", "l"),
        ("77", "m", "m"),
        ("78", "n", "n"),
        ("79", "o", "o"),
        ("80", "p", "p"),
        ("81", "q", "q"),
        ("82", "r", "r"),
        ("83", "s", "s"),
        ("84", "t", "t"),
        ("85", "u", "u"),
        ("86", "v", "v"),
        ("87", "w", "w"),
        ("88", "x", "x"),
        ("89", "y", "y"),
        ("90", "z", "z"),
        ("48", "0", "0"),
        ("49", "1", "1"),
        ("50", "2", "2"),
        ("51", "3", "3"),
        ("52", "4", "4"),
        ("53", "5", "5"),
        ("54", "6", "6"),
        ("55", "7", "7"),
        ("56", "8", "8"),
        ("57", "9", "9"),
        ("190", "Period", "Period"),
        ("188", "Comma", "Comma"),
        ("32", "Space", "Space"),
        ("8", "Backspace", "Backspace"),
        ("9", "Tab", "Tab"),
        ("13", "Enter", "Enter"),
        ("16", "Shift", "Shift"),
        ("17", "Control", "Control"),
        ("18", "Alt", "Alt"),
        ("27", "Escape", "Escape"),
        ("delete", "Delete", "Delete"),
        ("46", "Back", "Back"),
        ("38", "Up Arrow", "Up Arrow"),
        ("39", "Right Arrow", "Right Arrow"),
        ("37", "Left Arrow", "Left Arrow"),
        ("40", "Down Arrow", "Down Arrow")
    ], name="Key Code", default="32")

    def __init__(self):
        super()
        self.display_shape = ENUM_SHAPE

    def get_default_value(self):
        return int(self.keycode)

    def draw(self, context, layout, node, text):
        draw_socket_layout(self, "keycode", layout)

    def draw_color(self, context, node):
        return 0.24, 0.72, 0.83, 1


class ArmKouiSocketEvent(arm_sockets.ArmCustomSocket):
    """Represents an event."""
    bl_idname = "ArmKouiSocketEvent"
    bl_label = "Event Socket"
    arm_socket_type = 'NONE'

    def __init__(self):
        super()
        self.display_shape = ELEMENT_SHAPE

    def draw(self, context, layout, node, text):
        layout.label(text=self.name)

    def draw_color(self, context, node):
        # Same color as arm_sockets.ArmActionSocket
        return 0.8, 0.3, 0.3, 1


def draw_socket_layout(socket: bpy.types.NodeSocket, prop_name: str, layout: bpy.types.UILayout):
    if not socket.is_output and not socket.is_linked:
        layout = layout.split(factor=0.5, align=True)

    layout.label(text=socket.name)

    if not socket.is_output and not socket.is_linked:
        layout.prop(socket, prop_name, text="")


ArmKouiSocketInterfaceElement = arm_sockets._make_socket_interface('ArmKouiSocketInterfaceElement', 'ArmKouiSocketElement')
ArmKouiSocketInterfaceRadioGroup = arm_sockets._make_socket_interface('ArmKouiSocketInterfaceRadioGroup', 'ArmKouiSocketRadioGroup')
ArmKouiSocketInterfaceNumberType = arm_sockets._make_socket_interface('ArmKouiSocketInterfaceNumberType', 'ArmKouiSocketNumberType')
ArmKouiSocketInterfaceAnchor = arm_sockets._make_socket_interface('ArmKouiSocketInterfaceAnchor', 'ArmKouiSocketAnchor')
ArmKouiSocketInterfaceMouseButton = arm_sockets._make_socket_interface('ArmKouiSocketInterfaceMouseButton', 'ArmKouiSocketMouseButton')
ArmKouiSocketInterfaceKeyCode = arm_sockets._make_socket_interface('ArmKouiSocketInterfaceKeyCode', 'ArmKouiSocketKeyCode')
ArmKouiSocketInterfaceEvent = arm_sockets._make_socket_interface('ArmKouiSocketInterfaceEvent', 'ArmKouiSocketEvent')

__REG_CLASSES = (
    ArmKouiSocketInterfaceElement,
    ArmKouiSocketInterfaceRadioGroup,
    ArmKouiSocketInterfaceNumberType,
    ArmKouiSocketInterfaceAnchor,
    ArmKouiSocketInterfaceMouseButton,
    ArmKouiSocketInterfaceKeyCode,
    ArmKouiSocketInterfaceEvent,

    ArmKouiSocketAnchor,
    ArmKouiSocketElement,
    ArmKouiSocketEvent,
    ArmKouiSocketKeyCode,
    ArmKouiSocketMouseButton,
    ArmKouiSocketNumberType,
    ArmKouiSocketRadioGroup,
)
register, unregister = bpy.utils.register_classes_factory(__REG_CLASSES)
