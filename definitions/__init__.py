"""
Import all nodes
"""
import glob
from os.path import dirname, basename, isfile

import arm.logicnode

modules = glob.glob(dirname(__file__) + "/*.py")
__all__ = [basename(f)[:-3] for f in modules if isfile(f)]


def init_nodes():
    arm.logicnode.init_nodes(base_path=__path__, base_package=__package__, subpackages_only=True)
