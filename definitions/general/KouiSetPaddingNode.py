import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiSetPaddingNode(ArmLogicTreeNode):
    """Set the global padding values"""
    bl_idname = "LNKouiSetPaddingNode"
    bl_label = "Koui: Set Padding"
    bl_icon = "WINDOW"

    arm_category = "Koui: General"
    arm_version = 1

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "In")
        self.add_input("ArmKouiSocketElement", "Layout")
        self.add_input("NodeSocketInt", "Left")
        self.add_input("NodeSocketInt", "Right")
        self.add_input("NodeSocketInt", "Top")
        self.add_input("NodeSocketInt", "Bottom")
        self.add_output("ArmNodeSocketAction", "Out")
