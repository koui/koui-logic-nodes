import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiInitNode(ArmLogicTreeNode):
    """Initializes Koui"""
    bl_idname = "LNKouiInitNode"
    bl_label = "Koui: Init"
    bl_icon = "WINDOW"

    arm_category = "Koui: General"
    arm_version = 1

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "In")
        self.add_output("ArmNodeSocketAction", "Out")
        self.add_output("ArmKouiSocketElement", "AnchorPane")
