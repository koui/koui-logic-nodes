import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket

from arm.logicnode.arm_nodes import *


class KouiFontLocaleNode(ArmLogicTreeNode):
    """Settings regarding the font's locale.

    @see https://koui.gitlab.io/api/koui/utils/FontUtil.html"""
    bl_idname = "LNKouiFontLocaleNode"
    bl_label = "Koui: Font Locale"
    bl_icon = "WINDOW"

    arm_category = "Koui: General"
    arm_version = 1

    property0: EnumProperty(
        items=[
            ("load_glyphs_locale", "Load Glyphs From Locale", "Additionally load glyphs from the given locale"),
            ("unload_glyphs_locale", "Unload Glyphs From Locale", "Unload glyphs from the given locale. Glyphs that are loaded in another locale are not unloaded"),
        ],
        name="Action",
        description="The action to execute",
        default="load_glyphs_locale"
    )

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "In")
        self.add_input("NodeSocketString", "Locale Code")

        self.add_output("ArmNodeSocketAction", "Out")

    def draw_buttons(self, context, layout):
        layout.scale_y = 1.3
        layout.prop(self, "property0", text="")
