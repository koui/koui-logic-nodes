import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiEventOnKeyCodePressNode(ArmLogicTreeNode):
    """Unpacks the information stored in a given KeyCodePress event"""
    bl_idname = "LNKouiEventOnKeyCodePressNode"
    bl_label = "Koui: Event On KeyCodePress"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 2

    def arm_init(self, context):
        self.add_input("ArmKouiSocketEvent", "KeyCodePress Event")

        self.add_output("ArmNodeSocketAction", "Received")
        self.add_output("ArmNodeSocketAction", "KeyDown")
        self.add_output("ArmNodeSocketAction", "KeyHold")
        self.add_output("ArmKouiSocketElement", "Element")
        self.add_output("ArmKouiSocketKeyCode", "Key Code")

    def get_replacement_node(self, node_tree: bpy.types.NodeTree):
        if self.arm_version not in (0, 1):
            raise LookupError()

        return NodeReplacement.Identity(self)
