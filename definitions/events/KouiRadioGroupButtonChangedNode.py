import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiRadioGroupButtonChangedNode(ArmLogicTreeNode):
    """Creates a new koui.utils.RadioGroup"""
    bl_idname = "LNKouiRadioGroupButtonChangedNode"
    bl_label = "Koui: RadioGroup Button Changed"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 1

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "Register")
        self.add_input("ArmKouiSocketRadioGroup", "Radio Group")

        self.add_output("ArmNodeSocketAction", "On Button Changed")
        self.add_output("ArmKouiSocketElement", "Radio Button")
