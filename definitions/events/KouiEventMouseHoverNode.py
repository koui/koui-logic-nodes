import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiEventOnMouseHoverNode(ArmLogicTreeNode):
    """Unpacks the information stored in a given MouseHover event"""
    bl_idname = "LNKouiEventOnMouseHoverNode"
    bl_label = "Koui: Event On MouseHover"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 2

    def arm_init(self, context):
        self.add_input("ArmKouiSocketEvent", "MouseHover Event")

        self.add_output("ArmNodeSocketAction", "Received")
        self.add_output("ArmNodeSocketAction", "HoverStart")
        self.add_output("ArmNodeSocketAction", "HoverActive")
        self.add_output("ArmNodeSocketAction", "HoverEnd")
        self.add_output("ArmKouiSocketElement", "Element")
        self.add_output("NodeSocketBool", "Mouse Moved")

    def get_replacement_node(self, node_tree: bpy.types.NodeTree):
        if self.arm_version not in (0, 1):
            raise LookupError()

        return NodeReplacement.Identity(self)
