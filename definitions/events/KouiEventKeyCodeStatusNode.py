import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiEventOnKeyCodeStatusNode(ArmLogicTreeNode):
    """Unpacks the information stored in a given KeyCodeStatus event"""
    bl_idname = "LNKouiEventOnKeyCodeStatusNode"
    bl_label = "Koui: Event On KeyCodeStatus"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 2

    def arm_init(self, context):
        self.add_input("ArmKouiSocketEvent", "KeyCodeStatus Event")

        self.add_output("ArmNodeSocketAction", "Received")
        self.add_output("ArmNodeSocketAction", "KeyDown")
        self.add_output("ArmNodeSocketAction", "KeyUp")
        self.add_output("ArmKouiSocketElement", "Element")
        self.add_output("ArmKouiSocketKeyCode", "Key Code")

    def get_replacement_node(self, node_tree: bpy.types.NodeTree):
        if self.arm_version not in (0, 1):
            raise LookupError()

        return NodeReplacement.Identity(self)
