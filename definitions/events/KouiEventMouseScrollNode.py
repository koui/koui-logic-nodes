import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiEventOnMouseScrollNode(ArmLogicTreeNode):
    """Unpacks the information stored in a given MouseScroll event"""
    bl_idname = "LNKouiEventOnMouseScrollNode"
    bl_label = "Koui: Event On MouseScroll"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 1

    def arm_init(self, context):
        self.add_input("ArmKouiSocketEvent", "MouseScroll Event")

        self.add_output("ArmNodeSocketAction", "Received")
        self.add_output("ArmNodeSocketAction", "Active")
        self.add_output("ArmKouiSocketElement", "Element")
        self.add_output("NodeSocketInt", "Scroll Delta")
