import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiEqualsMouseButtonNode(ArmLogicTreeNode):
    """Checks whether two mouse buttons are equal."""
    bl_idname = "LNKouiEqualsMouseButtonNode"
    bl_label = "Koui: Equals Mouse Button"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 1

    def arm_init(self, context):
        self.add_input("ArmKouiSocketMouseButton", "Button A")
        self.add_input("ArmKouiSocketMouseButton", "Button B")

        self.add_output("NodeSocketBool", "Equals")
