import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiEventOnMouseClickNode(ArmLogicTreeNode):
    """Unpacks the information stored in a given MouseClick event"""
    bl_idname = "LNKouiEventOnMouseClickNode"
    bl_label = "Koui: Event On MouseClick"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 2

    def arm_init(self, context):
        self.add_input("ArmKouiSocketEvent", "MouseClick Event")

        self.add_output("ArmNodeSocketAction", "Received")
        self.add_output("ArmNodeSocketAction", "ClickStart")
        self.add_output("ArmNodeSocketAction", "ClickHold")
        self.add_output("ArmNodeSocketAction", "ClickEnd")
        self.add_output("ArmNodeSocketAction", "ClickCancelled")
        self.add_output("ArmKouiSocketElement", "Element")
        self.add_output("ArmKouiSocketMouseButton", "Mouse Button")

    def get_replacement_node(self, node_tree: bpy.types.NodeTree):
        if self.arm_version not in (0, 1):
            raise LookupError()

        return NodeReplacement.Identity(self)
