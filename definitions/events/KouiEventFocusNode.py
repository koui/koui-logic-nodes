import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiEventOnFocusNode(ArmLogicTreeNode):
    """Unpacks the information stored in a given Focus event"""
    bl_idname = "LNKouiEventOnFocusNode"
    bl_label = "Koui: Event On Focus"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 2

    def arm_init(self, context):
        self.add_input("ArmKouiSocketEvent", "Focus Event")

        self.add_output("ArmNodeSocketAction", "Received")
        self.add_output("ArmNodeSocketAction", "FocusGet")
        self.add_output("ArmNodeSocketAction", "FocusLoose")
        self.add_output("ArmKouiSocketElement", "Element")
        self.add_output("ArmKouiSocketMouseButton", "Mouse Button")

    def get_replacement_node(self, node_tree: bpy.types.NodeTree):
        if self.arm_version not in (0, 1):
            raise LookupError()

        return NodeReplacement.Identity(self)
