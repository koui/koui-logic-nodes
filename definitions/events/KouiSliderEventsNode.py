import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket

from arm.logicnode.arm_nodes import *


class KouiSliderEventsNode(ArmLogicTreeNode):
    """Listen to slider-specific events"""
    bl_idname = "LNKouiSliderEventsNode"
    bl_label = "Koui: Slider Events"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 2

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "Register")
        self.add_input("ArmKouiSocketElement", "Slider")

        self.add_output("ArmNodeSocketAction", "ValueChangeEvent")

    def get_replacement_node(self, node_tree: bpy.types.NodeTree):
        if self.arm_version not in (0, 1):
            raise LookupError()

        return NodeReplacement.Identity(self)
