import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiEventOnKeyCharPressNode(ArmLogicTreeNode):
    """Unpacks the information stored in a given KeyCharPress event"""
    bl_idname = "LNKouiEventOnKeyCharPressNode"
    bl_label = "Koui: Event On KeyCharPress"
    bl_icon = "WINDOW"

    arm_category = "Koui: Events"
    arm_version = 2

    def arm_init(self, context):
        self.add_input("ArmKouiSocketEvent", "KeyCharPress Event")

        self.add_output("ArmNodeSocketAction", "Received")
        self.add_output("ArmNodeSocketAction", "KeyHold")
        self.add_output("ArmKouiSocketElement", "Element")
        self.add_output("NodeSocketString", "Key Char")

    def get_replacement_node(self, node_tree: bpy.types.NodeTree):
        if self.arm_version not in (0, 1):
            raise LookupError()

        return NodeReplacement(
            'LNKouiEventOnKeyCharPressNode', self.arm_version, 'LNKouiEventOnKeyCharPressNode', 2,
            in_socket_mapping={0:0},
            out_socket_mapping={0:0, 1:1, 2:1, 3:2, 4:3}
        )
