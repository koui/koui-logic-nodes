from typing import List

import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket

from arm.logicnode.arm_nodes import *


class KouiGridLayoutAddElementNode(ArmLogicTreeNode):
    """Add an element to the given GridLayout."""
    bl_idname = "LNKouiGridLayoutAddElementNode"
    bl_label = "Koui: GridLayout Add Element"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_section = "layout"
    arm_version = 1

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "In")
        self.add_input("ArmKouiSocketElement", "GridLayout")
        self.add_input("ArmKouiSocketElement", "Element")
        self.add_input("ArmIntSocket", "Row")
        self.add_input("ArmIntSocket", "Column")
        self.add_input("ArmKouiSocketAnchor", "Anchor")

        self.add_output("ArmNodeSocketAction", "Out")
