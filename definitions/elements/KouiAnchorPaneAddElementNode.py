from typing import List

import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket

from arm.logicnode.arm_nodes import *


class KouiAnchorPaneAddElementNode(ArmLogicTreeNode):
    """Add an element to the given AnchorPane."""
    bl_idname = "LNKouiAnchorPaneAddElementNode"
    bl_label = "Koui: AnchorPane Add Element"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_section = "layout"
    arm_version = 1

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "In")
        self.add_input("ArmKouiSocketElement", "AnchorPane")
        self.add_input("ArmKouiSocketElement", "Element")
        self.add_input("ArmKouiSocketAnchor", "Anchor")

        self.add_output("ArmNodeSocketAction", "Out")
