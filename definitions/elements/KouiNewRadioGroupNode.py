import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiNewRadioGroupNode(ArmLogicTreeNode):
    """Creates a new koui.utils.RadioGroup"""
    bl_idname = "LNKouiNewRadioGroupNode"
    bl_label = "Koui: New RadioGroup"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_version = 1

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "In")
        self.add_output("ArmNodeSocketAction", "Out")
        self.add_output("ArmKouiSocketRadioGroup", "Radio Group")
