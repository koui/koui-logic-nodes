import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiCheckboxCheckedNode(ArmLogicTreeNode):
    """Set/get the checkboxes checked value"""
    bl_idname = "LNKouiCheckboxCheckedNode"
    bl_label = "Koui: Checkbox Checked"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_version = 1

    def arm_init(self, context):
        self.inputs.new("ArmNodeSocketAction", "In (Set)")
        self.inputs.new("ArmKouiSocketElement", "Checkbox")
        self.inputs.new("NodeSocketBool", "Checked")
        self.add_output("ArmNodeSocketAction", "Out (On Set)")
        self.add_output("NodeSocketBool", "Checked (Get)")
