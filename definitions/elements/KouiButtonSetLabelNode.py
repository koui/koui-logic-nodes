import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiButtonSetLabelNode(ArmLogicTreeNode):
    """Set the global padding values"""
    bl_idname = "LNKouiButtonSetLabelNode"
    bl_label = "Koui: Button Set Label"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_version = 1

    def arm_init(self, context):
        self.inputs.new("ArmNodeSocketAction", "In")
        self.inputs.new("ArmKouiSocketElement", "Button")
        self.inputs.new("NodeSocketString", "Label")
        self.add_output("ArmNodeSocketAction", "Out")
