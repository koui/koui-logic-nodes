import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket

from arm.logicnode.arm_nodes import *


class KouiSliderAttribsNode(ArmLogicTreeNode):
    """Set/get attributes of a slider element"""
    bl_idname = "LNKouiSliderAttribsNode"
    bl_label = "Koui: Slider Attributes"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_version = 1

    # Amount of non-dynamic input sockets, must be in the top of all
    # sockets
    NUM_STATIC_INS = 1

    def update_property0(self, context):
        self.update_sockets(context)

    property0: EnumProperty(
        items=[
            ("get_value", "Get Value", "The current value of the slider"),
            ("set_value", "Set Value", "The current value of the slider"),
            ("get_precision", "Get Precision", "The number of decimal places the value of this element has"),
            ("set_precision", "Set Precision", "The number of decimal places the value of this element has"),
        ],
        name="Attribute",
        description="The attribute to get/set",
        default="get_value",
        update=update_property0
    )

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "In")
        self.add_input("ArmKouiSocketElement", "Slider")
        self.add_input("NodeSocketFloat", "Value")
        self.add_input("NodeSocketFloat", "Precision")

        self.add_output("ArmNodeSocketAction", "Out")
        self.add_output("NodeSocketFloat", "Value")
        self.add_output("NodeSocketFloat", "Precision")

        self.update_sockets(context)

    def draw_buttons(self, context, layout):
        layout.scale_y = 1.3
        layout.prop(self, "property0", text="")

    def update_sockets(self, context):
        for i in range(4):
            self.inputs[i].hide = True
        for i in range(3):
            self.outputs[i].hide = True

        self.inputs["Slider"].hide = False

        if self.property0.startswith("set"):
            self.inputs["In"].hide = False
            self.outputs["Out"].hide = False

        if self.property0 == "get_value":
            self.outputs[1].hide = False

        elif self.property0 == "set_value":
            self.inputs[2].hide = False

        elif self.property0 == "get_precision":
            self.outputs[2].hide = False

        elif self.property0 == "set_precision":
            self.inputs[3].hide = False
