from typing import List

import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket

from arm.logicnode.arm_nodes import *


class KouiExpanderAddElementNode(ArmLogicTreeNode):
    """Add an element to the given Expander."""
    bl_idname = "LNKouiExpanderAddElementNode"
    bl_label = "Koui: Expander Add Element"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_section = "layout"
    arm_version = 1

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "In")
        self.add_input("ArmKouiSocketElement", "Expander")
        self.add_input("ArmKouiSocketElement", "Element")
        self.add_input("ArmIntSocket", "Index", default_value=-1)

        self.add_output("ArmNodeSocketAction", "Out")
