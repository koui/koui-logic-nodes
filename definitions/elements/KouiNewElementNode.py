import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *
import arm.node_utils as node_utils

from typing import List


class KouiNewElementNode(ArmLogicTreeNode):
    """Set the global padding values"""
    bl_idname = "LNKouiNewElementNode"
    bl_label = "Koui: New Element"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_version = 2

    # Amount of non-dynamic input sockets, must be in the top of all
    # sockets
    NUM_STATIC_INS = 1

    def update_property0(self, context):
        self.update_sockets(context)

    property0: EnumProperty(
        items=[
            ("AnchorPane", "AnchorPane", "koui.elements.layouts.AnchorPane"),
            ("Button", "Button", "koui.elements.Button"),
            ("Checkbox", "Checkbox", "koui.elements.Checkbox"),
            ("ColLayout", "ColLayout", "koui.elements.layouts.ColLayout"),
            ("Dropdown", "Dropdown", "koui.elements.Dropdown"),
            ("Expander", "Expander", "koui.elements.layouts.Expander"),
            ("GridLayout", "GridLayout", "koui.elements.layouts.GridLayout"),
            ("ImagePanel", "ImagePanel", "koui.elements.ImagePanel"),
            ("Label", "Label", "koui.elements.Label"),
            ("NumberInput", "NumberInput", "koui.elements.NumberInput"),
            ("Panel", "Panel", "koui.elements.Panel"),
            ("Progressbar", "Progressbar", "koui.elements.Progressbar"),
            ("RadioButton", "RadioButton", "koui.elements.RadioButton"),
            ("RowLayout", "RowLayout", "koui.elements.layouts.RowLayout"),
            ("ScrollPane", "ScrollPane", "koui.elements.layouts.ScrollPane"),
            ("Slider", "Slider", "koui.elements.Slider"),
            ("TextInput", "TextInput", "koui.elements.TextInput"),
            ("Custom", "Custom", "A custom type")
        ],
        name="Type",
        description="The element type",
        default="Button",
        update=update_property0
    )

    property1: StringProperty(
        name="Override Theme ID",
        description="If this field is not empty, the theme ID of this element is overriden after creation",
        default=""
    )

    property2: EnumProperty(
        items=[
            ("Left", "Left", "Slider values grow to the left, horizontal orientation"),
            ("Right", "Right", "Slider values grow to the right, horizontal orientation"),
            ("Up", "Up", "Slider values grow to the top, vertical orientation"),
            ("Down", "Down", "Slider values grow to the bottom, vertical orientation")
        ],
        name="Slider orientation",
        description="The orientation of the slider",
        default="Right"
    )

    def arm_init(self, context):
        self.add_input("ArmNodeSocketAction", "In")

        self.add_output("ArmNodeSocketAction", "Out")
        self.add_output("ArmKouiSocketEvent", "On Hover")
        self.add_output("ArmKouiSocketEvent", "On Focus")
        self.add_output("ArmKouiSocketEvent", "On Click")
        self.add_output("ArmKouiSocketEvent", "On Scroll")
        self.add_output("ArmKouiSocketEvent", "On KeyCharPress")
        self.add_output("ArmKouiSocketEvent", "On KeyCodePress")
        self.add_output("ArmKouiSocketEvent", "On KeyCodeStatus")
        self.add_output("ArmKouiSocketElement", self.property0)

        self.update_sockets(context)

    def update_sockets(self, context):
        # Remove dynamically placed input sockets
        remove_list = []
        for i in range(KouiNewElementNode.NUM_STATIC_INS, len(self.inputs)):
            remove_list.append(self.inputs[i])
        for i in remove_list:
            self.inputs.remove(i)

        # Add dynamic input sockets
        if self.property0 == "NumberInput":
            self.add_input("ArmKouiSocketNumberType", "Number Type")

        if self.property0 == "RadioButton":
            self.add_input("ArmKouiSocketRadioGroup", "Radio Group")

        if self.property0 in ("Button", "Checkbox", "Dropdown", "RadioButton", "NumberInput"):
            self.add_input("NodeSocketString", "Label")

        elif self.property0 == "ImagePanel":
            self.add_input("NodeSocketString", "Image Name")

        elif self.property0 == "Label":
            self.add_input("NodeSocketString", "Text")

        elif self.property0 == "Progressbar" or self.property0 == "Slider":
            self.add_input("NodeSocketFloatUnsigned", "Min Value")
            self.add_input("NodeSocketFloatUnsigned", "Max Value")

        elif self.property0 == "Custom":
            self.add_input("NodeSocketString", "Type")

        # Layouts
        elif self.property0 in ("AnchorPane", "ColLayout", "Expander", "GridLayout", "RowLayout", "ScrollPane"):
            self.add_input("NodeSocketInt", "X Position")
            self.add_input("NodeSocketInt", "Y Position")
            self.add_input("NodeSocketInt", "Width")
            self.add_input("NodeSocketInt", "Height")

            if self.property0 == "ColLayout":
                self.add_input("NodeSocketIntUnsigned", "Column Amount")

            elif self.property0 == "Expander":
                self.add_input("ArmStringSocket", "Direction", default_value="down")

            elif self.property0 == "GridLayout":
                self.add_input("NodeSocketIntUnsigned", "Row Amount")
                self.add_input("NodeSocketIntUnsigned", "Column Amount")

            elif self.property0 == "RowLayout":
                self.add_input("NodeSocketIntUnsigned", "Row Amount")

        # Change element socket name
        if self.property0 != "Custom":
            self.outputs[8].name = self.property0
        else:
            self.outputs[8].name = "Element"

    def draw_buttons(self, context, layout):
        layout.prop(self, "property0")
        layout.prop(self, "property1", text="tID")

        if self.property0 == "Slider":
            layout.prop(self, "property2", text="Orientation")

        if self.property0 == "Custom":
            layout.label(text="This option is not yet implemented!", icon="ERROR")

    def get_replacement_node(self, node_tree: bpy.types.NodeTree):
        if self.arm_version not in (0, 2):
            raise LookupError()

        newnode = node_tree.nodes.new('LNKouiNewElementNode')
        node_utils.copy_basic_node_props(from_node=self, to_node=newnode)

        newnode.property0 = self.property0 # This is important to not replace by enum index...
        newnode.property1 = self.property1
        newnode.property2 = self.property2

        for idx, inp in enumerate(self.inputs):
            NodeReplacement.replace_input_socket(node_tree, inp, newnode.inputs[idx])

        for idx, out in enumerate(self.outputs):
            NodeReplacement.replace_output_socket(node_tree, out, newnode.outputs[idx])

        return newnode
