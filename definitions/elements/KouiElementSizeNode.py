import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiElementSizeNode(ArmLogicTreeNode):
    """Set/get the element's size"""
    bl_idname = "LNKouiElementSizeNode"
    bl_label = "Koui: Element Size"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_version = 1

    # Amount of non-dynamic input sockets, must be in the top of all
    # sockets
    NUM_STATIC_INS = 1

    def update_property0(self, context):
        self.update_sockets(context)

    property0: EnumProperty(
        items=[
            ("get", "Get Size", "The current value of the slider"),
            ("set", "Set Size", "The current value of the slider"),
        ],
        name="Mode",
        description="Choose whether to get or set the size",
        default="set",
        update=update_property0
    )

    def arm_init(self, context):
        self.inputs.new("ArmNodeSocketAction", "In")
        self.inputs.new("ArmKouiSocketElement", "Element")
        self.inputs.new("NodeSocketInt", "Width")
        self.inputs.new("NodeSocketInt", "Height")

        self.add_output("ArmNodeSocketAction", "Out")
        self.add_output("NodeSocketInt", "Width")
        self.add_output("NodeSocketInt", "Height")

        self.update_sockets(context)

    def draw_buttons(self, context, layout):
        layout.scale_y = 1.3
        layout.prop(self, "property0", text="")

    def update_sockets(self, context):
        for i in range(4):
            self.inputs[i].hide = True
        for i in range(3):
            self.outputs[i].hide = True

        self.inputs["Element"].hide = False

        if self.property0 == "set":
            self.inputs["In"].hide = False
            self.inputs["Width"].hide = False
            self.inputs["Height"].hide = False
            self.outputs["Out"].hide = False

        elif self.property0 == "get":
            self.outputs["Width"].hide = False
            self.outputs["Height"].hide = False
