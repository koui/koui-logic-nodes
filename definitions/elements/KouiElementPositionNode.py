import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiElementPositionNode(ArmLogicTreeNode):
    """Set/get the element's position"""
    bl_idname = "LNKouiElementPositionNode"
    bl_label = "Koui: Element Position"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_version = 1

    def arm_init(self, context):
        self.inputs.new("ArmNodeSocketAction", "In (Set)")
        self.inputs.new("ArmKouiSocketElement", "Element")
        self.inputs.new("NodeSocketInt", "X Position")
        self.inputs.new("NodeSocketInt", "Y Position")
        self.add_output("ArmNodeSocketAction", "Out (On Set)")
        self.add_output("NodeSocketInt", "Get X Pos")
        self.add_output("NodeSocketInt", "Get Y Pos")
