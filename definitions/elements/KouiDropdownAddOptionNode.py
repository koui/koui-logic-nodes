import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiDropdownAddOptionNode(ArmLogicTreeNode):
    """Adds an option to the given dropdown menu."""
    bl_idname = "LNKouiDropdownAddOptionNode"
    bl_label = "Koui: Dropdown Add Option"

    arm_category = "Koui: Elements"
    arm_version = 1

    def arm_init(self, context):
        self.inputs.new("ArmNodeSocketAction", "In")
        self.inputs.new("ArmKouiSocketElement", "Dropdown")
        self.inputs.new("NodeSocketString", "Option")

        self.add_output("ArmNodeSocketAction", "Out")
