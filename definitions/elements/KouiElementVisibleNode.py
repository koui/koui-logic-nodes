import bpy
from bpy.props import *
from bpy.types import Node, NodeSocket
from arm.logicnode.arm_nodes import *


class KouiElementVisibleNode(ArmLogicTreeNode):
    """Set/get whether the element is visible"""
    bl_idname = "LNKouiElementVisibleNode"
    bl_label = "Koui: Element Visible"
    bl_icon = "WINDOW"

    arm_category = "Koui: Elements"
    arm_version = 1

    def arm_init(self, context):
        self.inputs.new("ArmNodeSocketAction", "In (Set)")
        self.inputs.new("ArmKouiSocketElement", "Element")
        self.inputs.new("NodeSocketBool", "Visible")
        self.add_output("ArmNodeSocketAction", "Out (On Set)")
        self.add_output("NodeSocketBool", "Visible (Get)")
