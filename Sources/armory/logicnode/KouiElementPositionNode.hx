package armory.logicnode;

import koui.elements.Element;

class KouiElementPositionNode extends LogicNode {
	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var element: Null<Element> = inputs[1].get();
		if (element == null) {
			return;
		}

		element.setPosition(inputs[2].get(), inputs[3].get());
		runOutput(0);
	}

	override function get(from: Int) {
		var element: Null<Element> = inputs[1].get();
		if (element == null) return 0;

		switch (from) {
			case 2: return element.posX;
			case 3: return element.posY;
			default:
				throw 'get() was called with invalid "from" parameter: $from!';
				return 0;
		}
	}
}
