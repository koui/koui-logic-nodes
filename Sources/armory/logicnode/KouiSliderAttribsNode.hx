package armory.logicnode;

import koui.elements.Slider;

class KouiSliderAttribsNode extends LogicNode {
	// Attribute mode
	public var property0: String = "";

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var slider: Null<Slider> = inputs[1].get();

		switch (property0) {
			case "set_value":
				if (slider != null) {
					slider.value = inputs[2].get();
				}
			case "set_precision":
				if (slider != null) {
					slider.precision = inputs[3].get();
				}
			default:
				trace('run() was called with wrong configuration: $property0');
				return;
		}

		runOutput(0);
	}

	override function get(from: Int): Dynamic {
		var slider: Null<Slider> = inputs[1].get();
		if (slider == null) return 0;

		if (from == 1) return slider.value;
		if (from == 2) return slider.precision;
		else {
			throw 'get() was called with invalid "from" parameter: $from!';
			return null;
		}
	}
}
