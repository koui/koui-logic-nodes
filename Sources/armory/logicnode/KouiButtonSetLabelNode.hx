package armory.logicnode;

import koui.Koui;
import koui.elements.Button;

class KouiButtonSetLabelNode extends LogicNode {
	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var button: Button = inputs[1].get();
		if (button != null) {
			button.text = inputs[2].get();
		}

		runOutput(0);
	}
}
