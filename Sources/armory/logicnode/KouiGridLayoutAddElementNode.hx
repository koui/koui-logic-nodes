package armory.logicnode;

import koui.Koui;
import koui.elements.Element;
import koui.elements.layouts.GridLayout;
import koui.elements.layouts.Layout.Anchor;


class KouiGridLayoutAddElementNode extends LogicNode {
	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var gridLayout: Null<GridLayout> = inputs[1].get();
		var element: Null<Element> = inputs[2].get();
		var row: Null<Int> = inputs[3].get();
		var column: Null<Int> = inputs[4].get();

		if (gridLayout == null || element == null || row == null || column == null) {
			return;
		}

		var anchor: Anchor = switch(inputs[5].get()) {
			case "TopLeft": TopLeft;
			case "TopCenter": TopCenter;
			case "TopRight": TopRight;
			case "MiddleLeft": MiddleLeft;
			case "MiddleCenter": MiddleCenter;
			case "MiddleRight": MiddleRight;
			case "BottomLeft": BottomLeft;
			case "BottomCenter": BottomCenter;
			case "BottomRight": BottomRight;
			case _:
				trace(inputs[5].get());
				throw "Invalid anchor type!";
		}

		gridLayout.add(element, row, column, anchor);

		runOutput(0);
	}
}
