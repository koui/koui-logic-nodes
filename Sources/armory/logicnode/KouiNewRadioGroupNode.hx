package armory.logicnode;

import koui.utils.RadioGroup;

class KouiNewRadioGroupNode extends LogicNode {
	var radioGroup: RadioGroup;

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		radioGroup = new RadioGroup();
		runOutput(0);
	}

	override function get(from: Int) {
		return radioGroup;
	}
}
