package armory.logicnode;

import koui.elements.Checkbox;

class KouiCheckboxCheckedNode extends LogicNode {
	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var checkbox: Null<Checkbox> = inputs[1].get();
		if (checkbox != null) {
			checkbox.isChecked = inputs[2].get();
		}

		runOutput(0);
	}

	override function get(from: Int): Dynamic {
		var checkbox: Null<Checkbox> = inputs[1].get();
		if (checkbox == null) return null;

		if (from == 1) return checkbox.isChecked;
		else {
			throw 'get() was called with invalid "from" parameter: $from!';
			return null;
		}
	}
}
