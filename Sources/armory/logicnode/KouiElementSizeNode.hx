package armory.logicnode;

import koui.elements.Element;

class KouiElementSizeNode extends LogicNode {
	// Attribute mode
	public var property0: String = "";

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var element: Null<Element> = inputs[1].get();

		if (element != null) {
			element.setSize(inputs[2].get(), inputs[3].get());
			runOutput(0);
		}
	}

	override function get(from: Int): Dynamic {
		var element: Null<Element> = inputs[1].get();
		if (element == null) return 0;

		if (from == 1) return element.width;
		if (from == 2) return element.height;
		else {
			throw 'get() was called with invalid "from" parameter: $from!';
			return null;
		}
	}
}
