package armory.logicnode;

import koui.elements.Element;

class KouiElementVisibleNode extends LogicNode {
	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var element: Null<Element> = inputs[1].get();

		if (element != null) {
			element.visible = inputs[2].get();
		}

		runOutput(0);
	}

	override function get(from: Int): Dynamic {
		var element: Null<Element> = inputs[1].get();
		if (element == null) return null;

		if (from == 2) return element.visible;
		else {
			throw 'get() was called with invalid "from" parameter: $from!';
			return null;
		}
	}
}
