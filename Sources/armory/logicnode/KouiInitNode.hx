package armory.logicnode;

import koui.Koui;

class KouiInitNode extends LogicNode {

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		Koui.init(() -> {
			tree.notifyOnRender2D((g2: kha.graphics2.Graphics) -> {
				g2.end();
				Koui.render(g2);
				g2.begin(false);
			});

			runOutput(0);
		});
	}

	override function get(from: Int) {
		return @:privateAccess Koui.anchorPane;
	}
}
