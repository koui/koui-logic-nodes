package armory.logicnode;

import koui.utils.FontUtil;

class KouiFontLocaleNode extends LogicNode {
	// Mode
	public var property0: String = "";

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var locale: String = inputs[1].get();

		switch (property0) {
			case "load_glyphs_locale":
				FontUtil.loadGlyphsFromLocale(locale);

			case "unload_glyphs_locale":
				FontUtil.unloadGlyphsFromLocale(locale);

			default:
				trace('run() was called with wrong configuration: $property0');
				return;
		}

		runOutput(0);
	}
}
