package armory.logicnode;

import koui.Koui;
import koui.elements.Element;
import koui.events.KeyEvent.KeyCodeStatusEvent;

class KouiEventOnKeyCodeStatusNode extends LogicNode {
	var element: Element;
	var keyCode: Null<kha.input.KeyCode>;

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var event: KeyCodeStatusEvent = inputs[0].get();
		if (event == null) return;

		this.element = event.element;
		this.keyCode = event.keyCode;
		// Reset on next frame
		tree.notifyOnUpdate(this.reset);

		switch (event.getState()) {
			case KeyDown: runOutput(1);
			case KeyUp: runOutput(2);
			default:
				throw 'Invalid KeyCodeStatus event state: ${event.getState()}!';
		}

		runOutput(0);
	}

	function reset() {
		this.element = null;
		this.keyCode = null;
		tree.removeUpdate(this.reset);
	}

	override function get(from: Int): Dynamic {
		switch (from) {
			case 3: return this.element;
			case 4: return this.keyCode;
			default:
				throw 'get() was called with invalid "from" parameter: $from!';
				return null;
		}
	}
}
