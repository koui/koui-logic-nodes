package armory.logicnode;

import koui.Koui;
import koui.elements.Element;
import koui.events.KeyEvent.KeyCharPressEvent;

class KouiEventOnKeyCharPressNode extends LogicNode {
	var element: Element;
	var keyChar: String;

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var event: KeyCharPressEvent = inputs[0].get();
		if (event == null) return;

		this.element = event.element;
		this.keyChar = event.keyChar;
		// Reset on next frame
		tree.notifyOnUpdate(this.reset);

		switch (event.getState()) {
			case KeyHold: runOutput(1);
			default:
				throw 'Invalid KeyCharPress event state: ${event.getState()}!';
		}

		runOutput(0);
	}

	function reset() {
		this.element = null;
		this.keyChar = null;
		tree.removeUpdate(this.reset);
	}

	override function get(from: Int): Dynamic {
		switch (from) {
			case 3: return this.element;
			case 4: return this.keyChar;
			default:
				throw 'get() was called with invalid "from" parameter: $from!';
				return null;
		}
	}
}
