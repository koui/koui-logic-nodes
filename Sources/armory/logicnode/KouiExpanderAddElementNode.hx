package armory.logicnode;

import koui.Koui;
import koui.elements.Element;
import koui.elements.layouts.Expander;


class KouiExpanderAddElementNode extends LogicNode {
	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var expander: Null<Expander> = inputs[1].get();
		var element: Null<Element> = inputs[2].get();
		var index: Null<Int> = inputs[3].get();

		if (expander == null || element == null || index == null) {
			return;
		}

		if (index == -1) {
			index == null;
		}
		expander.add(element, index);

		runOutput(0);
	}
}
