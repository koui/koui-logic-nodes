package armory.logicnode;

import koui.Koui;
import koui.elements.layouts.Layout;

class KouiSetPaddingNode extends LogicNode {

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var i = 1;
		var layout: Null<Layout> = inputs[i++].get();
		var pLeft = inputs[i++].get();
		var pRight = inputs[i++].get();
		var pTop = inputs[i++].get();
		var pBottom = inputs[i++].get();

		if (layout == null) {
			Koui.setPadding(pLeft, pRight, pTop, pBottom);
		} else {
			layout.setPadding(pLeft, pRight, pTop, pBottom);
		}

		runOutput(0);
	}
}
