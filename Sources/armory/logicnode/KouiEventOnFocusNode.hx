package armory.logicnode;

import koui.Koui;
import koui.elements.Element;
import koui.events.FocusEvent;

class KouiEventOnFocusNode extends LogicNode {
	var element: Element;
	var mouseButton: Null<MouseButton>;

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var event: FocusEvent = inputs[0].get();
		if (event == null) return;

		this.element = event.element;
		this.mouseButton = event.mouseButton;
		// Reset on next frame
		tree.notifyOnUpdate(this.reset);

		switch (event.getState()) {
			case FocusGet: runOutput(1);
			case FocusLoose: runOutput(2);
			default:
				throw 'Invalid Focus event state: ${event.getState()}!';
		}

		runOutput(0);
	}

	function reset() {
		this.element = null;
		this.mouseButton = null;
		tree.removeUpdate(this.reset);
	}

	override function get(from: Int): Dynamic {
		switch (from) {
			case 3: return this.element;
			case 4: return this.mouseButton;
			default:
				throw 'get() was called with invalid "from" parameter: $from!';
				return null;
		}
	}
}
