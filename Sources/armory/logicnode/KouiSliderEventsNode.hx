package armory.logicnode;

import koui.elements.Slider;
import koui.events.ValueChangeEvent;

class KouiSliderEventsNode extends LogicNode {
    public function new(tree: LogicTree) {
        super(tree);
    }

    override function run(from: Int) {
        var slider: Null<Slider> = inputs[1].get();
        if (slider == null) return;

        slider.addEventListener(ValueChangeEvent, (e: ValueChangeEvent) -> {
            runOutput(0);
        });
    }
}
