package armory.logicnode;

import koui.Koui;
import koui.elements.Element;
import koui.events.MouseEvent.MouseScrollEvent;

class KouiEventOnMouseScrollNode extends LogicNode {
	var element: Element;
	var scrollDelta = 0;

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var event: MouseScrollEvent = inputs[0].get();
		if (event == null) return;

		this.element = event.element;
		this.scrollDelta = event.scrollDelta;
		// Reset on next frame
		tree.notifyOnUpdate(this.reset);

		switch (event.getState()) {
			case Scroll: runOutput(1);
			default:
				throw 'Invalid MouseScroll event state: ${event.getState()}!';
		}

		runOutput(0);
	}

	function reset() {
		this.element = null;
		this.scrollDelta = 0;
		tree.removeUpdate(this.reset);
	}

	override function get(from: Int): Dynamic {
		switch (from) {
			case 2: return this.element;
			case 3: return this.scrollDelta;
			default:
				throw 'get() was called with invalid "from" parameter: $from!';
				return null;
		}
	}
}
