package armory.logicnode;

import koui.elements.RadioButton;
import koui.utils.RadioGroup;

class KouiRadioGroupButtonChangedNode extends LogicNode {
	var lastRadioButton: Null<RadioButton> = null;

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var radioGroup: RadioGroup = inputs[0].get();
		if (radioGroup == null) return;

		radioGroup.onButtonChanged((button: RadioButton) -> {
			lastRadioButton = button;
			runOutput(0);
		});
	}

	override function get(from: Int) {
		return lastRadioButton;
	}
}
