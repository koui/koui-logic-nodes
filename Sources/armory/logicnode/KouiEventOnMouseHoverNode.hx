package armory.logicnode;

import koui.Koui;
import koui.elements.Element;
import koui.events.MouseEvent.MouseHoverEvent;

class KouiEventOnMouseHoverNode extends LogicNode {
	var element: Element;
	var mouseMoved = false;

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var event: MouseHoverEvent = inputs[0].get();
		if (event == null) return;

		this.element = event.element;
		this.mouseMoved = event.mouseMoved;
		// Reset on next frame
		tree.notifyOnUpdate(this.reset);

		switch (event.getState()) {
			case HoverStart: runOutput(1);
			case HoverActive: runOutput(2);
			case HoverEnd: runOutput(3);
			default:
				throw 'Invalid MouseHover event state: ${event.getState()}!';
		}

		runOutput(0);
	}

	function reset() {
		this.element = null;
		this.mouseMoved = null;
		tree.removeUpdate(this.reset);
	}

	override function get(from: Int): Dynamic {
		switch (from) {
			case 4: return this.element;
			case 5: return this.mouseMoved;
			default:
				throw 'get() was called with invalid "from" parameter: $from!';
				return null;
		}
	}
}
