package armory.logicnode;

import koui.Koui;
import koui.elements.*;
import koui.elements.Slider.SliderOrientation;
import koui.elements.NumberInput.NumberType;
import koui.elements.layouts.*;
import koui.elements.layouts.Expander.ExpanderDirection;
import koui.events.Event;
import koui.events.FocusEvent;
import koui.events.KeyEvent;
import koui.events.MouseEvent;


class KouiNewElementNode extends LogicNode {

	/* Element type */
	public var property0: String;

	/* Override theme id */
	public var property1: String;

	/* Slider orientation */
	public var property2: String;

	var element: Null<Element> = null;

	var eventMouseHover: Event;
	var eventFocus: Event;
	var eventMouseClick: Event;
	var eventMouseScroll: Event;
	var eventKeyCharPress: Event;
	var eventKeyCodePress: Event;
	var eventKeyCodeStatus: Event;

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		switch (property0) {
			case "AnchorPane":
				element = new AnchorPane(inputs[1].get(), inputs[2].get(), inputs[3].get(), inputs[4].get());

			case "Button":
				element = new Button(inputs[1].get());

			case "Checkbox":
				element = new Checkbox(inputs[1].get());

			case "ColLayout":
				element = new ColLayout(inputs[1].get(), inputs[2].get(), inputs[3].get(), inputs[4].get(), inputs[5].get());

			case "Expander":
				final direction: ExpanderDirection = switch ((inputs[5].get(): String).toLowerCase()) {
					case "up": UP;
					case "down": DOWN;
					case "left": LEFT;
					case "right": RIGHT;
					default: throw "Unsupported expander direction!";
				}
				element = new Expander(inputs[1].get(), inputs[2].get(), inputs[3].get(), inputs[4].get(), direction);

			case "Dropdown":
				element = new Dropdown(inputs[1].get());

			case "GridLayout":
				element = new GridLayout(inputs[1].get(), inputs[2].get(), inputs[3].get(), inputs[4].get(), inputs[5].get(), inputs[6].get());

			case "ImagePanel":
				var assetName: String = inputs[1].get();
				var image = kha.Assets.images.get(assetName);

				if (image != null) {
					element = new ImagePanel(image);
				}
				else {
					kha.Assets.loadImage(assetName, (img: kha.Image) -> {
						if (img != null) {
							// Call this here, loadImage() is asynchronous
							this.element = new ImagePanel(img);
							registerListeners();
							runOutput(0);
							return;
						}

						throw 'Could not load image "$assetName", please check that it exists!';
					});

					// Don't call registerListeners() and runOutput() twice
					return;
				}

			case "Label":
				element = new Label(inputs[1].get());

			case "NumberInput":
				var numberType = switch(inputs[1].get()) {
					case "TypeInt":
						NumberType.TypeInt;
					case "TypeUnsignedInt":
						NumberType.TypeUnsignedInt;
					case "TypeFloat":
						NumberType.TypeFloat;
					case "TypeUnsignedFloat":
						NumberType.TypeUnsignedFloat;
					case _:
						trace(inputs[1].get());
						throw "Invalid number type!";
				};
				element = new NumberInput(numberType, inputs[2].get());

			case "Panel":
				element = new Panel();

			case "Progressbar":
				element = new Progressbar(inputs[1].get(), inputs[2].get());

			case "RadioButton":
				element = new RadioButton(inputs[1].get(), inputs[2].get());

			case "RowLayout":
				element = new RowLayout(inputs[1].get(), inputs[2].get(), inputs[3].get(), inputs[4].get(), inputs[5].get());

			case "ScrollPane":
				element = new ScrollPane(inputs[1].get(), inputs[2].get(), inputs[3].get(), inputs[4].get());

			case "Slider":
				var orientation: SliderOrientation = switch (property2) {
					case "Left": Left;
					case "Right": Right;
					case "Up": Up;
					case "Down": Down;
					default: throw "Internal error!";
				}
				element = new Slider(inputs[1].get(), inputs[2].get(), orientation);

			case "TextInput":
				element = new TextInput(inputs[1].get());

			case "Custom":
				// Todo
				return;
		}

		if (property1 != "") {
			element.setTID(property1);
		}

		registerListeners();
		runOutput(0);
	}

	function registerListeners() {
		element.addEventListener(MouseHoverEvent, (e: MouseHoverEvent) -> {
			eventMouseHover = e;
			runOutput(1);
			tree.notifyOnUpdate(resetMouseHoverEvent);
		});
		element.addEventListener(FocusEvent, (e: FocusEvent) -> {
			eventFocus = e;
			runOutput(2);
			tree.notifyOnUpdate(resetFocusEvent);
		});
		element.addEventListener(MouseClickEvent, (e: MouseClickEvent) -> {
			eventMouseClick = e;
			runOutput(3);
			tree.notifyOnUpdate(resetMouseClickEvent);
		});
		element.addEventListener(MouseScrollEvent, (e: MouseScrollEvent) -> {
			eventMouseScroll = e;
			runOutput(4);
			tree.notifyOnUpdate(resetMouseScrollEvent);
		});
		element.addEventListener(KeyCharPressEvent, (e: KeyCharPressEvent) -> {
			eventKeyCharPress = e;
			runOutput(5);
			tree.notifyOnUpdate(resetKeyCharPressEvent);
		});
		element.addEventListener(KeyCodePressEvent, (e: KeyCodePressEvent) -> {
			eventKeyCodePress = e;
			runOutput(6);
			tree.notifyOnUpdate(resetKeyCodePressEvent);
		});
		element.addEventListener(KeyCodeStatusEvent, (e: KeyCodeStatusEvent) -> {
			eventKeyCodeStatus = e;
			runOutput(7);
			tree.notifyOnUpdate(resetKeyCodeStatusEvent);
		});
	}

	function resetMouseHoverEvent() { eventMouseHover = null; tree.removeUpdate(resetMouseHoverEvent); }
	function resetFocusEvent() { eventFocus = null; tree.removeUpdate(resetFocusEvent); }
	function resetMouseClickEvent() { eventMouseClick = null; tree.removeUpdate(resetMouseClickEvent); }
	function resetMouseScrollEvent() { eventMouseScroll = null; tree.removeUpdate(resetMouseScrollEvent); }
	function resetKeyCharPressEvent() { eventKeyCharPress = null; tree.removeUpdate(resetKeyCharPressEvent); }
	function resetKeyCodePressEvent() { eventKeyCodePress = null; tree.removeUpdate(resetKeyCodePressEvent); }
	function resetKeyCodeStatusEvent() { eventKeyCodeStatus = null; tree.removeUpdate(resetKeyCodeStatusEvent); }

	override function get(from: Int): Dynamic {
		switch (from) {
			case 1: return eventMouseHover;
			case 2: return eventFocus;
			case 3: return eventMouseClick;
			case 4: return eventMouseScroll;
			case 5: return eventKeyCharPress;
			case 6: return eventKeyCodePress;
			case 7: return eventKeyCodeStatus;
			case 8: return this.element;
			default:
				throw 'get() was called with invalid "from" parameter: $from!';
				return null;
		}

	}
}
