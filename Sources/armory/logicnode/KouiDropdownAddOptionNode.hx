package armory.logicnode;

import koui.elements.Dropdown;

class KouiDropdownAddOptionNode extends LogicNode {
    public function new(tree: LogicTree) {
        super(tree);
    }

    override function run(from: Int) {
        var checkbox: Dropdown = inputs[1].get();
        if (checkbox != null) {
            checkbox.addOption(inputs[2].get());
        }
    	runOutput(0);
    }
}
