package armory.logicnode;

import koui.Koui;
import koui.elements.Element;
import koui.elements.layouts.AnchorPane;
import koui.elements.layouts.Layout.Anchor;


class KouiAnchorPaneAddElementNode extends LogicNode {
	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var anchorPane: Null<AnchorPane> = inputs[1].get();
		var element: Null<Element> = inputs[2].get();

		if (anchorPane == null || element == null) {
			return;
		}

		var anchor: Anchor = switch(inputs[3].get()) {
			case "TopLeft": TopLeft;
			case "TopCenter": TopCenter;
			case "TopRight": TopRight;
			case "MiddleLeft": MiddleLeft;
			case "MiddleCenter": MiddleCenter;
			case "MiddleRight": MiddleRight;
			case "BottomLeft": BottomLeft;
			case "BottomCenter": BottomCenter;
			case "BottomRight": BottomRight;
			case _:
				trace(inputs[3].get());
				throw "Invalid anchor type!";
		}

		anchorPane.add(element, anchor);

		runOutput(0);
	}
}
