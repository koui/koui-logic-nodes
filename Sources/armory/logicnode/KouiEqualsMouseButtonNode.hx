package armory.logicnode;

class KouiEqualsMouseButtonNode extends LogicNode {
	public function new(tree: LogicTree) {
		super(tree);
	}

	override function get(from: Int): Dynamic {
		return inputs[0].get() == inputs[1].get();
	}
}
