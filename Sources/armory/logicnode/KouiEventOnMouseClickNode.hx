package armory.logicnode;

import koui.Koui;
import koui.elements.Element;
import koui.events.MouseEvent.MouseButton;
import koui.events.MouseEvent.MouseClickEvent;

class KouiEventOnMouseClickNode extends LogicNode {
	var element: Element;
	var mouseButton: Null<MouseButton>;

	public function new(tree: LogicTree) {
		super(tree);
	}

	override function run(from: Int) {
		var event: MouseClickEvent = inputs[0].get();
		if (event == null) return;

		this.element = event.element;
		this.mouseButton = event.mouseButton;
		// Reset on next frame
		tree.notifyOnUpdate(this.reset);

		switch (event.getState()) {
			case ClickStart: runOutput(1);
			case ClickHold: runOutput(2);
			case ClickEnd: runOutput(3);
			case ClickCancelled: runOutput(4);
			default:
				throw 'Invalid MouseClick event state: ${event.getState()}!';
		}

		runOutput(0);
	}

	function reset() {
		this.element = null;
		this.mouseButton = null;
		tree.removeUpdate(this.reset);
	}

	override function get(from: Int): Dynamic {
		switch (from) {
			case 5: return this.element;
			case 6: return this.mouseButton;
			default:
				throw 'get() was called with invalid "from" parameter: $from!';
				return null;
		}
	}
}
