import bpy

from arm.logicnode.arm_nodes import *

# Register all nodes
from definitions import *
import definitions


def register():
    definitions._sockets.register()

    add_category_section("koui")

    add_category("Koui: General", icon="WINDOW", section="koui")

    cat = add_category("Koui: Elements", icon="WINDOW", section="koui")
    cat.add_node_section("default")
    cat.add_node_section("layout")

    add_category("Koui: Events", icon="WINDOW", section="koui")

    definitions.init_nodes()


def unregister():
    definitions._sockets.unregister()
